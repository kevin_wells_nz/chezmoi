function fish_prompt
	# save $status first
	set -l last_status $status
	set stat (set_color normal) '> '
	if test $last_status -ne 0
		set stat (set_color red) '> '
	end

	set directory (set_color blue) (prompt_pwd --full-length-dirs 3)

	set -g __fish_git_prompt_showuntrackedfiles 1
	set -g __fish_git_prompt_showdirtystate 1
	set -g __fish_git_prompt_showupstream auto
	set -g __fish_git_prompt_showstashstate auto
	set git (set_color green) (fish_git_prompt)

	set cmd_duration (math -s0 $CMD_DURATION / 1000)
	set duration ""
	if test $cmd_duration -ge 3
		set duration (string join -- (set_color yellow) " " $cmd_duration "s")
	end

	echo ""
	string join '' -- $directory $git $duration
	echo $stat
end
